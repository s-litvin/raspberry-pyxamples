# Random blinking on GPIO's. 
# Connect your LED (with some resistors) to GPIO14 and GPIO15.
# Run programm with command: python3 blink.py
# Pres ctrl+c to terminate the programm.

import RPi.GPIO as GPIO
import time
from random import random, randrange, uniform

# set to use GPIO numbers (or GPIO.BOARD to use pin numbers)
GPIO.setmode(GPIO.BCM)

# set up GPIO output channel  
GPIO.setup(14, GPIO.OUT)
GPIO.setup(15, GPIO.OUT)   

while True:
	GPIO.output(14, randrange(0,2,1))
	GPIO.output(15, randrange(0,2,1))
	time.sleep(uniform(0, 3)/10)

GPIO.cleanup()
