import urllib.request, json
import time
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)
GPIO.setup(18, GPIO.OUT) 

while True:
	response = urllib.request.urlopen("https://api.thingspeak.com/channels/205926/fields/1.json?results=1").read()
	data = json.loads(response.decode())

	if data['feeds'][0]['field1'] == '1':
		for i in range(1, 4):
			for i in range(1, 10):
				GPIO.output(18, 1)
				time.sleep(0.05)
				GPIO.output(18, 0)
				time.sleep(0.05)
			time.sleep(0.7)
	else:
		time.sleep(10)
		GPIO.output(18, 0)
